const christmas = false;


if (christmas) {
    document.querySelector('body').setAttribute('id', 'christmas');

    document.addEventListener('DOMContentLoaded', () => {
        const snowflakesContainer = document.querySelector('#modifiers');
        const snowflakeCount = 100; // Number of snowflakes
    
        for (let i = 0; i < snowflakeCount; i++) {
            const snowflake = document.createElement('div');
            snowflake.classList.add('snowflake');
    
            // Random size for each snowflake
            const size = Math.random() * 10 + 5 + 'px';
            snowflake.style.width = size;
            snowflake.style.height = size;
    
            // Random position
            snowflake.style.left = Math.random() * 100 + 'vw';
    
            // Random duration and delay for the fall animation
            const duration = Math.random() * 5 + 5 + 's';
            const delay = Math.random() * 10 + 's';
            snowflake.style.animationDuration = duration;
            snowflake.style.animationDelay = delay;
    
            // Append to the container
            snowflakesContainer.appendChild(snowflake);
        }
    });


}