function getCurrentTime() {
    const now = new Date();
    let hours = now.getHours();
    let minutes = now.getMinutes();
    let seconds = now.getSeconds();

    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    return `${hours}:${minutes}:${seconds}`;
}

const updateTime = document.getElementById('latest-update');

const time = document.getElementById('time');

time.textContent = getCurrentTime();

// Update clock
setInterval(() => {
    time.textContent = getCurrentTime();
}, 1000);

// Reload webpage every 12 hours to dump memory & calibrate timers.
setTimeout(() => {
    location.reload();
}, 60_000 * 60 * 12);


const pages = ['minecraft.php', 'roosters.php', 'checkins.php', 'discord-events.php', 'weer.php', 'discord-announcements.php'];
let index = 0;
const display = document.getElementById('display');

function carousel() {
    const currentPage = pages[index];

    fetch('http://localhost/' + currentPage)
        .then(res => res.text())
        .then(text => {
            display.innerHTML = text;
        });

    updateTime.textContent = getCurrentTime();

    if (index >= pages.length - 1) {
        index = 0;
    } else {
        index++;
    }
};

setInterval(() => {
    carousel();
}, 30_000);
carousel();