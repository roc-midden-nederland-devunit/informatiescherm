<h2 class="text-center fw-bold mb-4">⚒️ Speel mee op de DevUnit Minecraft server</h2>

<div class="row justify-content-center">
  <div class="col-md-10">
    <h2 class="text-center mb-4">
      <kbd class="p-2">play.pijlwagen.dev</kbd>
    </h2>

    <iframe src="https://play.pijlwagen.dev/?worldname=world&mapname=surface&zoom=2&x=22&y=64&z=-219" width="100%" height="800" frameborder="0" class="rounded"></iframe>
  </div>
</div>