<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

require __DIR__ . '/vendor/autoload.php';

use Carbon\Carbon;

Carbon::setLocale('nl');

$classes = json_decode(file_get_contents('roosters.json'), true);
$now = Carbon::now('Europe/Amsterdam');

$colors = [
    "RO21S" => "#2cb555",
    "RO31S" => "#8b1ebe",
    "RO31T" => "#1ebeab",
    "RO23S" => "#1e79bf",
    "RO11S" => "#be7b1e",
    "RO21T" => "#ddc131",
    "RO41S" => "#96d747",
    "RO41T" => "#e74c3c",
    "RO31X" => "#6c5ce7",
    "RO31"  => "#8B1EBE"
];

?>

<h1 class="text-center fw-bold display-5 mb-4">📅 Roosters</h1>

<div class="row justify-content-center" id="roosters">
    <?php foreach ($classes as $class => $schedule) : ?>
        <?php

        $today = $schedule[$now->dayOfWeekIso] ?? null;
        $text = 'vrij';
        $after = null;

        if ($today) {
            $index = 0;
            $blocksToday = count($today) - 1; // compensate, arrays start at 0

            if ($now->isBefore('08:30')) {
                $text = 'Inchecken vanaf 08:30';

                if ($blocksToday > 0) {
                    $nextBlock = $today[0];
                    $after = '<b>' .  $nextBlock['text'] . '</b>';

                    if ($nextBlock['tutor'] ?? null) {
                        $after .= ' van ' . $nextBlock['tutor'];
                    }

                    if ($nextBlock['location'] ?? null) {
                        $after .= ' in <b>' . $nextBlock['location'] . '</b>';
                    }

                    $after .= ' tot ' . $nextBlock['end'];
                }
            } else {
                foreach ($today as $block) {
                    [$startHours, $startMinutes] = explode(':', $block['start']);
                    $start = Carbon::parse($now)->setHours(intval($startHours))->setMinutes(intval($startMinutes));
                    [$endHours, $endMinutes] = explode(':', $block['end']);
                    $end = Carbon::parse($now)->setHours(intval($endHours))->setMinutes(intval($endMinutes));


                    if ($now->between($start, $end)) {
                        $text = '<b>' .  $block['text'] . '</b>';

                        if ($block['tutor'] ?? null) {
                            $text .= ' van ' . $block['tutor'];
                        }

                        if ($block['location'] ?? null) {
                            $text .= ' in <b>' . $block['location'] . '</b>';
                        }

                        $text .= ' tot ' . $end->format('H:i');

                        if ($today[$index + 1] ?? null) {
                            $nextBlock = $today[$index + 1];
                            $after = '<b>' .  $nextBlock['text'] . '</b>';

                            if ($nextBlock['tutor'] ?? null) {
                                $after .= ' van ' . $nextBlock['tutor'];
                            }

                            if ($nextBlock['location'] ?? null) {
                                $after .= ' in <b>' . $nextBlock['location'] . '</b>';
                            }

                            $after .= ' tot ' . $nextBlock['end'];
                        } else {
                            $after = 'vrij';
                        }
                    }

                    $index++;

                    if ($index === (count($today) - 1) && $text === 'vrij') {
                        $text = 'Er zijn geen lessen meer vandaag.';
                    }
                }
            }
        }

        ?>
        <?php if ($today) : ?>
            <div class="col-4 mb-4 d-flex align-items-stretch">
                <div class="card shadow-sm border-0 w-100 d-flex h-100 w-100">
                    <?php if ($_SESSION['config']['christmas']): ?>
                        <ul class="strand">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    <?php endif; ?>
                    <div class="card-body my-auto">
                        <h1 class="my-auto text-center">
                            <span class="badge text-white" style="background: <?= $colors[$class] ?> !important">
                                <?= $class ?>
                            </span>
                        </h1>
                        <div class="d-flex flex-column ms-3">
                            <?php if ($after) : ?>
                                <span>nu: <?= $text ?></span>
                                <span>daarna: <?= $after ?></span>
                            <?php else : ?>
                                <span class="my-auto"><?= $text ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>