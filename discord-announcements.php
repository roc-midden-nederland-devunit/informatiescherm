<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/tokens.php';
include __DIR__ . '/functions.php';

use Carbon\Carbon;
use cebe\markdown\Markdown;
use GuzzleHttp\Client;

$mentions = [
    '<@&1262446727508656229>' => 'Jaar 1',
    '<@&1195031190873325568>' => 'Jaar 2',
    '<@&1195031211538657320>' => 'Jaar 3',
    '<@&1195031240768761957>' => 'Klas van 2024',
    '<@&1205068448946724865>' => 'Stagelopers',
    '<@&1197823243898339378>' => 'RO21T',
    '<@&1195032354226450462>' => 'RO21S',
    '<@&1195030974468210719>' => 'RO23S',
    '<@&1195031096639881246>' => 'RO31S',
    '<@&1195031032152461394>' => 'RO31T',
    '<@&1280869992362934405>' => 'RO41S',
    '<@&1280870066031693855>' => 'RO41T',
    '<@&1195030695429537822>' => 'Student',
    '<@&1195030845946335312>' => 'Klassenvertegenwoordiger',
    '<@&1207586379798814720>' => 'Moderator',
    '<@&1195030791898542171>' => 'Coach',
    '@everyone' => 'everyone',
];

$admins = [
    '689773989110743150' => 'Melanie',
    '778444708845846548' => 'Michel',
    '1197894303859622020' => 'Aziz',
    '377546194408439808' => 'Ewout',
    '772591122827968522' => 'Jim',
    '1199320768971030619' => 'Thomas',
    '1198958683405692998' => 'Wytze'
];

$client = new Client(['verify' => false]);
$announcements = [];

try {
    $response = $client->get('https://discord.com/api/v10/channels/1195021555667849350/messages?limit=3', [
        'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bot ' . DISCORD_TOKEN
        ]
    ]);

    $announcements = json_decode($response->getBody(), true);

    $announcements = array_filter($announcements, function ($message) {
        $timestamp = Carbon::now()->diffInMonths(Carbon::parse($message['timestamp']));
        return $timestamp <= 0;
    });

    $announcements = array_map(function ($message) {
        $content = replaceMentions($message['content']);

        if (strlen($content) > 1024) {
            $content = substr($content, 0, 1024) . "... (zie discord voor meer informatie)";
        }

        return [
            'content' => $content,
            'author' => $message['author'],
            'timestamp' => $message['timestamp']
        ];
    }, $announcements);
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

?>
<h2 class="text-center fw-bold mb-4">📢 Mededelingen</h2>

<?php if (count($announcements) > 0) : ?>
    <div class="row">
        <div class="col-8 mx-auto">
            <?php
            $i = 0;
            foreach ($announcements as $message) : ?>
                <?php if ($message['content']) : ?>
                    <div class="card shadow-sm border-0 mb-2" id="announcements">
                        <div class="card-body">

                            <b><?= $admins[$message['author']['id']] ?? $message['author']['global_name'] ?></b> <small class="text-muted"><?= Carbon::parse($message['timestamp'], 'UTC')->timezone('Europe/Amsterdam')->translatedFormat('d-m-y H:i'); ?></small>
                            <?= (new Markdown)->parse($message['content']); ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php $i++;
            endforeach; ?>
        </div>
    </div>
<?php else : ?>
    <div class="text-center my-5">
        <img src="/assets/news.svg" alt="" class="img-fluid" style="max-width: 500px;">
        <h3 class="mt-3">Er zijn momenteel geen mededelingen</h3>
    </div>
<?php endif; ?>