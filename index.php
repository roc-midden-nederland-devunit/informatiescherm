<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

$_SESSION['config'] = [
    'christmas' => false,
    'halloween' => false
]
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Informatiescherm DevUnit</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Anta&family=Nunito:ital,wght@0,200..1000;1,200..1000&family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style.css">
    <?php if ($_SESSION['config']['halloween']) echo '<link rel="stylesheet" href="assets/halloween.css">'; ?>
    <?php if ($_SESSION['config']['christmas']): ?>
        <link rel="stylesheet" href="assets/kerst/kersemus.css">
        <link rel="stylesheet" href="assets/kerst/lights.css">
    <?php endif; ?>
</head>

<body>
    <div id="app">
        <div class="text-center d-flex shadow-sm px-2 mb-4" id="header">
            <img src="assets/dev-unit-logo.png" alt="" height="100">
            <h1 class="mx-auto my-auto">Developers Unit</h1>
            <h1 class="text-muted fw-bold my-auto" id="time"></h1>
        </div>
        <?php if ($_SESSION['config']['christmas']) echo '<div id="ice"></div>'; ?>
        <div class="container-fluid px-3">
            <div id="display"></div>
        </div>

        <footer class="text-center mt-auto">
            <small class="text-muted">Laatste update: <span id="latest-update"></span></small>
        </footer>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script src="assets/script.js"></script>
    <script src="assets/modifiers.js"></script>

    <?php if ($_SESSION['config']['halloween']) echo '<script src="assets/halloween.js"></script>' ?>
    <?php if ($_SESSION['config']['christmas']): ?>
        <script src="assets/kerst/snowflakes.js"></script>
        <script>
            const colors = ['#f2f4ff', '#b9d9fb', '#96c5f7', '#76a6d8', '#aee1ec', '#beeef9', '#ffffff', '#f4fbfb', '#e9f7f9', '#fefdfd'];
            const maxSnowFlakes = 90;

            for (const color of colors) {
                new Snowflakes({
                    count: Math.round(maxSnowFlakes / colors.length),
                    color: color,
                    wind: true
                });
            }
        </script>
    <?php endif; ?>
</body>

</html>