<?php

define('ROLES', [
    '<@&1262446727508656229>' => 'Jaar 1',
    '<@&1195031190873325568>' => 'Jaar 2',
    '<@&1195031211538657320>' => 'Jaar 3',
    '<@&1195031240768761957>' => 'Klas van 2024',
    '<@&1205068448946724865>' => 'Stagelopers',
    '<@&1197823243898339378>' => 'RO21T',
    '<@&1195032354226450462>' => 'RO21S',
    '<@&1195030974468210719>' => 'RO23S',
    '<@&1195031096639881246>' => 'RO31S',
    '<@&1195031032152461394>' => 'RO31T',
    '<@&1280869992362934405>' => 'RO41S',
    '<@&1280870066031693855>' => 'RO41T',
    '<@&1195030695429537822>' => 'Student',
    '<@&1195030845946335312>' => 'Klassenvertegenwoordiger',
    '<@&1207586379798814720>' => 'Moderator',
    '<@&1195030791898542171>' => 'Coach',
    '@everyone' => 'everyone',
    '<@&1245141085479632996>' => 'Laravel'
]);

define('CHANNELS', [
    '<#1249617535649845289>' => '#nexed'
]);

define('ADMINS', [
    '689773989110743150' => 'Melanie',
    '778444708845846548' => 'Michel',
    '1197894303859622020' => 'Aziz',
    '377546194408439808' => 'Ewout',
    '772591122827968522' => 'Jim',
    '1199320768971030619' => 'Thomas',
    '1198958683405692998' => 'Wytze'
]);

function replaceMentions($string)
{
    foreach (ROLES as $mention => $role) {
        $string = str_replace($mention, "<span class='text-primary'>@$role</span>", $string);
    }

    foreach (CHANNELS as $id => $name) {
        $string = str_replace($id, "<span class='text-primary'>$name</span>", $string);
    }

    foreach (ADMINS as $id => $name) {
        $string = preg_replace("/<@$id>/", "<span class='text-primary'>@$name</span>", $string);
    }

    $string = preg_replace('/http.*\?event=[0-9]+/', 'zie <span class="text-primary">Discord</span> voor evenement details.', $string);

    preg_match_all("/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/", $string, $matches);

    foreach ($matches ?? [] as $match) {
        foreach ($match as $url) {
            if ($url) $string = str_replace($url, "<u class='text-primary'>$url</u>", $string);
        }
    }

    return $string;
}
