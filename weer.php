<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/tokens.php';

use Carbon\Carbon;
use GuzzleHttp\Client;

Carbon::setLocale('nl');

$client = new Client(['verify' => false]);

$forecast = null;

try {
    $response = $client->get('http://api.weatherapi.com/v1/forecast.json?key=' . WEATHER_TOKEN . '&q=Nieuwegein&days=2&aqi=yes&alerts=yes');

    $forecast = json_decode($response->getBody(), true);

    // $forecast = json_decode(file_get_contents('event.json'), true);
} catch (Exception $e) {
    echo $e->getMessage();
}

if (!$forecast) die('Unable to connect to the Weather API.');

$now = $forecast['current'];
$today = $forecast['forecast']['forecastday'][0];
$tomorrow = $forecast['forecast']['forecastday'][1];

?>

<h2 class="text-center fw-bold mb-4">⛅ Weersvoorspelling</h2>

<div class="row">
    <div class="col-8 mx-auto">
        <div class="row justify-content-center">
            <div class="col-5">
                <div class="card border-0 shadow-sm">
                    <?php if ($_SESSION['config']['christmas']): ?>
                        <ul class="strand">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    <?php endif; ?>
                    <div class="card-body">
                        <div class="card-title d-flex">
                            <h3 class="me-auto my-auto">Vandaag</h3>
                            <h3 class="my-auto"><?= ceil($today['day']['maxtemp_c']) ?> &deg;C</h3>
                            <img src="<?= $today['day']['condition']['icon'] ?>" alt="Weather API">
                        </div>
                        <div><b>Wind</b>: <?= ceil($today['day']['maxwind_kph']) ?>km/h</div>
                        <div><b>Neerslag</b>: <?= $today['day']['totalprecip_mm'] ?>mm</div>
                    </div>
                </div>
            </div>
            <div class="col-5">
                <div class="card border-0 shadow-sm">
                    <?php if ($_SESSION['config']['christmas']): ?>
                        <ul class="strand">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    <?php endif; ?>
                    <div class="card-body">
                        <div class="card-title d-flex">
                            <h3 class="me-auto my-auto">Morgen</h3>
                            <h3 class="my-auto"><?= ceil($tomorrow['day']['maxtemp_c']); ?> &deg;C</h3>
                            <img src="<?= $tomorrow['day']['condition']['icon'] ?>" alt="Weather API">
                        </div>
                        <div><b>Wind</b>: <?= ceil($tomorrow['day']['maxwind_kph']) ?>km/h</div>
                        <div><b>Neerslag</b>: <?= $tomorrow['day']['totalprecip_mm'] ?>mm</div>
                    </div>
                </div>
            </div>
            <div class="col-5 mt-4">
                <div class="card border-0 shadow-sm">
                    <?php if ($_SESSION['config']['christmas']): ?>
                        <ul class="strand">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    <?php endif; ?>
                    <div class="card-body">
                        <div class="card-title d-flex">
                            <h3 class="me-auto my-auto">Nu</h3>
                            <h3 class="my-auto"><?= $now['temp_c'] ?> &deg;C</h3>
                            <img src="<?= $now['condition']['icon'] ?>" alt="Weather API">
                        </div>
                        <div><b>Wind</b>: <?= ceil($now['wind_kph']) ?>km/h (tot <?= ceil($now['gust_kph']) ?>km/h) </div>
                        <div><b>Neerslag</b>: <?= $now['precip_mm'] ?>mm</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-center my-5">
    <iframe src="https://gadgets.buienradar.nl/gadget/zoommap/?lat=52.04379&lng=5.09674&overname=2&zoom=8&naam=3438ED&size=3&voor=1" scrolling=no width=550 height=512 frameborder=no></iframe>
</div>