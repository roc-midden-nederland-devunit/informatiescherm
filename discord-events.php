<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/tokens.php';
include __DIR__ . '/functions.php';

use Carbon\Carbon;
use GuzzleHttp\Client;

Carbon::setLocale('nl');

$client = new Client(['verify' => false]);
$events = [];

try {
    $response = $client->get('https://discord.com/api/v10/guilds/1195021555206459552/scheduled-events', [
        'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bot ' . DISCORD_TOKEN
        ]
    ]);

    $events = json_decode($response->getBody(), true);
    usort($events, function ($a, $b) {
        $dateA = Carbon::parse($a['scheduled_start_time']);
        $dateB = Carbon::parse($b['scheduled_start_time']);

        return $dateA->gt($dateB) ? 1 : -1;
    });
} catch (Exception $e) {
    var_dump($e);
}
?>

<h2 class="text-center fw-bold mb-4">📆 Aankomende activiteiten</h2>

<?php if (count($events) > 0) : ?>
    <div class="row">
        <div class="col-8 mx-auto">
            <div class="row justify-content-center">
                <?php
                $i = 0;
                foreach ($events as $event) :
                    if ($i >= 4) break;
                ?>
                    <div class="col-6">
                        <div class="card mb-2 shadow-sm border-0">
                            <?php if ($_SESSION['config']['christmas']): ?>
                                <ul class="strand">
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                            <?php endif; ?>
                            <?php if ($event['image']) : ?>
                                <img src="https://cdn.discordapp.com/guild-events/<?= $event['id'] ?>/<?= $event['image'] ?>.png?size=4096" alt="" class="card-img-top">
                            <?php endif; ?>
                            <div class="card-body">
                                <h3 class="card-title"><?= $event['name'] ?></h3>
                                <?php if ($event['description']) {
                                    $event['description'] = replaceMentions($event['description']);
                                    if (strlen($event['description']) > 64)  $event['description'] = substr($event['description'], 0, 64) . '... (zie discord voor meer informatie)';
                                    // echo $event['description'];
                                }
                                ?>

                                <div>
                                    ⏰
                                    <span><?= ucfirst(Carbon::parse(str_replace('+00:00', '', $event['scheduled_start_time']), 'UTC')->timezone('Europe/Amsterdam')->translatedFormat('l j F Y \\o\\m H:i')); ?></span>
                                </div>

                                <?php if (array_key_exists('entity_metadata', $event) && array_key_exists('location', $event['entity_metadata'])) : ?>
                                    <div>
                                        📍 <?= $event['entity_metadata']['location']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php $i++;
                endforeach;
                ?>
            </div>
        </div>
    </div>
<?php else : ?>
    <div class="text-center my-5">
        <img src="/assets/events.svg" alt="" class="img-fluid" style="max-width: 500px;">
        <h3 class="mt-3">Er staan momenteel geen activiteiten op de planning</h3>
    </div>
<?php endif; ?>